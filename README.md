# Wardrobify

Team:

* Person 1: Shiran Xiao - Which microservice? Shoes
* Person 2: Key Gomez - Which microservice? Hats


## Design

Design is in Design.png

## Shoes microservice

Explain your models and integration with the wardrobe microservice, here.

My Shoe model is containing manufacturer, model name, color, a URL for a picture, and the bin information in the wardrobe where it exists. We use poller to poll bin information from wardrobe microservice and stored as value object.

## Hats microservice

My microservice will work to allow a list of hats with their details to be shown. The user will also be able to create a new hat or delete an existing one. It will do so with RESTful APIs and being displayed by React components.

It is important to make sure you have Docker, Git and Django.

1. Fork the Repository: https://gitlab.com/xiao.shiran/microservice-two-shot

2. Clone the forked repository onto your local computer using the git clone command

3. Build and run the project using Docker with the commands:
docker volume create beta-data
docker-compose build
docker-compose up

after doing so make sure your containers are running in docker desktop

you can view the project in the browser: http://localhost:3000/


## API Documentation

### URLs and Ports
Hats uses the port 8090
The url for hats will be http://localhost:8090/api/hats
include an id number to delete or update hats

Shoes uses the port 8080
The url for shoes will be http://localhost:8080/api/shoes
include an id number to delete or update shoes

### Shoe API
Shoe API is built to store, create and update data about shoes in a bin, which is then stored in your wardrobe. To do so it makes use of the value object  "BinVO" to get information on where a specific shoe is located from the Wardrobe API. The model for "BinVO" is in models.py, which also has the "Shoe" model that is used to contain the properties of: manufacturer, model name, color, a picture url, and, using the BinVO model, bin.

These models are then used by the views in views.py to know what data should be displayed. It also allows for the creation of new sheoss, the updating of properties of an existing shoe and the deletion of an existing shoe.


### Hats API

Hats API is built to store, create and update data about hats in a location, which is then stored in your wardrobe. To do so it makes use of the value object  "LocationVO" to get information on where a specific hat is located from the Wardrobe API. The model for "LocationVO" is in models.py, which also has the "Hat" model that is used to contain the properties of: fabric, style name, color, a picture url, and, using the LocationVO model, location.

These models are then used by the views in views.py to know what data should be displayed. It also allows for the creation of new hats, the updating of properties of an existing hat and the deletion of an existing hat.

## Value Objects
Hats contains the Value object "LocationVO"

Shoes contains the Value object "BinVO"
