import React, { useEffect, useState } from "react";

function HatForm() {
    const [fabric, setFabric] = useState('');
    const [styleName, setStyleName] = useState('');
    const [color, setColor] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');
    const [location, setLocation] = useState('');
    const [locations, setLocations] = useState([]);
    const [hasCreatedHat, setHasCreatedHat] = useState(false);

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.fabric = fabric;
        data.style_name = styleName;
        data.color = color;
        data.picture_url = pictureUrl;
        data.location = location;

        const hatUrl = 'http://localhost:8090/api/hats';
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        try {
            const hatResponse = await fetch(hatUrl, fetchOptions);

            if (hatResponse.ok) {
                setFabric('');
                setStyleName('');
                setColor('');
                setPictureUrl('');
                setLocation('');
                setHasCreatedHat(false);
            } else {
                console.error('Failed to create hat:', hatResponse.statusText);
            }
        } catch (error) {
            console.error('Fetch error:', error);
        }
    };

    const handleChangeFabric = (event) => {
        const value = event.target.value;
        setFabric(value);
    }

    const handleChangeStyleName = (event) => {
        const value = event.target.value;
        setStyleName(value);
    }

    const handleChangeColor = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handleChangePictureUrl = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }

    const handleChangeLocation = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    let spinnerClasses = 'd-flex justify-content-center mb-3';
    let dropdownClasses = 'form-select d-none';
    if (locations.length > 0) {
        spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
        dropdownClasses = 'form-select';
    }

    let messageClasses = 'alert alert-success d-none mb-0';
    let formClasses = '';
    if (hasCreatedHat) {
        messageClasses = 'alert alert-success mb-0';
        formClasses = 'd-none'
    }
    return (
        <div className="my-5 container">
            <div className="row">
                <div className="col">
                    <div className="card shadow">
                        <div className="card-body">
                            <form className={formClasses} onSubmit={handleSubmit} id="create-hat-form">
                                <h1 className="card-title">It's Hat Time!</h1>
                                <p className="mb-3">
                                    Please choose which location
                                    you'd like to store the hat in.
                                </p>
                                <div className={spinnerClasses} id="loading-location-spinner">
                                    <span className="visually-hidden">Loading...</span>
                                </div>

                                <p className="mb-3">
                                Please enter the hat details.
                                </p>
                                    <div className="row">
                                        <div className="row">
                                            <div className="form-floating mb-3">
                                                <input onChange={handleChangeFabric} value={fabric} required placeholder="Fabric" type="text" id="fabric" name="fabric" className="form-control" />
                                            <label htmlFor="name">Fabric type</label>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="form-floating mb-3">
                                            <input onChange={handleChangeStyleName} value={styleName} required placeholder="Style" type="text" id="style" name="style" className="form-control" />
                                            <label htmlFor="name">Style type</label>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="form-floating mb-3">
                                            <input onChange={handleChangeColor} value={color} required placeholder="Color" type="text" id="color" name="color" className="form-control" />
                                            <label htmlFor="name">Color</label>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="form-floating mb-3">
                                            <input onChange={handleChangePictureUrl} value={pictureUrl} required placeholder="Pic_Url" type="text" id="pic_url" name="pic_url" className="form-control" />
                                            <label htmlFor="name">Picture URL</label>
                                            </div>
                                        <div className="row">
                                            <div className="mb-3">
                                            <select onChange={handleChangeLocation} value={location} name="location" id="location" className={dropdownClasses} required>
                                            <option value="">Choose a location</option>
                                            {locations.map(location => {
                                            return (
                                                <option key={location.href} value={location.href}>{location.closet_name}</option>
                                            )
                                        })}
                                    </select>
                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <button className="btn btn-lg btn-primary">Store that hat!</button>
                            </form>
                            <div className={messageClasses} id="success-message">
                                Congratulations! Your hat has been created!
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default HatForm;
