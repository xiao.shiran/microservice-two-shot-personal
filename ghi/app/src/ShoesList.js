import { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';


function ShoesList() {
    const [shoes, setShoes] = useState([])

    const getData = async () => {
      const response = await fetch('http://localhost:8080/api/shoes/');


      if (response.ok) {
        const data = await response.json();
        setShoes(data.shoes)
      }
    }

    useEffect(()=>{
      getData()
    }, [])


    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Manufacturer</th>
            <th>Model Name</th>
            <th>Color</th>
            <th>Picture</th>
            <th>
                <NavLink className="nav-link active" aria-current="page" to={`/shoes/new/`}>
                    <button variant="primary">Create New Shoe</button>
                </NavLink>
            </th>
          </tr>
        </thead>
        <tbody>
          {shoes.map(shoe => {
            return (
                    <tr key={shoe.id} >
                        <td>{ shoe.manufacturer }</td>
                        <td>{ shoe.model_name }</td>
                        <td>{ shoe.color }</td>
                        <td>
                            <img
                                src={shoe.picture_url}
                                width={100} height={100}
                                alt='shoe picture'
                            />
                        </td>
                        <td>
                            <NavLink className="nav-link active" aria-current="page" to={`/shoes/${shoe.id}/`}>
                                <button >Detail</button>
                            </NavLink>
                        </td>
                    </tr>
            );
          })}
        </tbody>
      </table>
    );
}

export default ShoesList
